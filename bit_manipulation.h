#ifndef BIT_MANIPULATION_H
#define BIT_MANIPULATION_H

#define CROSSOVER_RATE            0.7
#define MUTATION_RATE             0.001
#define NUM_CHROMOSOMES           10//must be an even number
#define CHROMOSOME_LENGTH         15
#define MAX_ALLOWABLE_GENERATIONS 400

typedef unsigned char byte;

typedef struct chromo_t
{
    byte* bits;
    double fitness;
} chromosome;

extern void set_bit( int* A, int k );
extern void  clear_bit( int* A, int k );
extern int get_bit( int* A, int k );
extern byte get_gene( byte* number, int n );
extern void print_bit( byte c );
extern void print_chromosome( byte* chromosome );
#endif
