#include "bit_manipulation.h"
#include <stdio.h>

void set_bit( int* A, int k )
{
   int i = k/sizeof(int);
   int pos = k%sizeof(int);

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   A[i] = A[i] | flag;     // Set the bit at the k-th position in A[i]
}

void  clear_bit( int* A, int k )
{
   int i = k/sizeof(int);
   int pos = k%sizeof(int);

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)
   flag = ~flag;           // flag = 1111...101..111

   A[i] = A[i] & flag;     // RESET the bit at the k-th position in A[i]
}

int get_bit( int* A, int k )
{
   int i = k/sizeof(int);
   int pos = k%sizeof(int);

   unsigned int flag = 1;  // flag = 0000.....00001

   flag = flag << pos;     // flag = 0000...010...000   (shifted k positions)

   if ( A[i] & flag )      // Test the bit at the k-th position in A[i]
      return 1;
   else
      return 0;
}

void print_bit( byte c )
{
    int i;
    for (i = 0; i < 8; i++) {
      printf("%d", !!((c << i) & 0x80));
    }
    printf("\n"); 
}

byte get_gene( byte * chromosome, int n )
{
    byte x = chromosome[ (int)(n / 2) ];
    int shift = n % 2;

    byte c = (byte)(( x >> (4 * shift) ) & 0xf);
    return c;
}

void print_chromosome( byte* chromosome )
{
    byte ret_str[CHROMOSOME_LENGTH * 2];

    for( int i = 0; i < CHROMOSOME_LENGTH * 2; i++ ) // two genes per char
    {
        byte gene = get_gene( chromosome, i );

        if( gene == 0 )
        {
            ret_str[i] = '0';
        }
        else if( gene == 1 )
        {
            ret_str[i] = '1';
        }
        else if( gene == 2 )
        {
            ret_str[i] = '2';
        }
        else if( gene == 3 )
        {
            ret_str[i] = '3';
        }
        else if( gene == 4 )
        {
            ret_str[i] = '4';
        }
        else if( gene == 5 )
        {
            ret_str[i] = '5';
        }
        else if( gene == 6 )
        {
            ret_str[i] = '6';
        }
        else if( gene == 7 )
        {
            ret_str[i] = '7';
        }
        else if( gene == 8 )
        {
            ret_str[i] = '8';
        }
        else if( gene == 9 )
        {
            ret_str[i] = '9';
        }
        else if( gene == 10 )
        {
            ret_str[i] = '+';
        }
        else if( gene == 11 )
        {
            ret_str[i] = '-';
        }
        else if( gene == 12 )
        {
            ret_str[i] = '*';
        }
        else if( gene == 13 )
        {
            ret_str[i] = '/';
        }

        if( gene <= 13 && gene >= 0 ) printf("%c",ret_str[i]);
    }
    printf( "\n" );
}

