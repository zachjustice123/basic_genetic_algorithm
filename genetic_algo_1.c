#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "bit_manipulation.h"

byte* get_chromosome()
{
    byte* chromosome = malloc(CHROMOSOME_LENGTH * sizeof(byte));

    int i = 1;

    chromosome[0] = (byte)(rand() % 10); // first element in chromosome must be number

    while( i < CHROMOSOME_LENGTH )
    {
        byte x, y;

        x = (byte)( rand() % 0xA );
        y = (byte)( (  rand() % 0x4 ) + 10);

        chromosome[i++] = ( x << 4 ) | y;

        //print_bit(x);
    }

    return chromosome;
}

int evaluate_fitness( int target_val, byte* chromosome )
{
    double sum = 0;
    int curr_val = 0;
    int last_op = 10; // start off by adding first number to zero

    for( int i = 0; i < CHROMOSOME_LENGTH * 2; i++ )
    {
        byte gene = get_gene( chromosome, i );

        if( gene < 10 )
        {
            curr_val = ( curr_val * 10 ) + (int)gene;
        }
        else
        {
            switch( last_op )
            {
                case 10 :
                    //printf("%f + %d\n", sum, curr_val);
                    sum += curr_val;
                    break;
                case 11 :
                    //printf("%f - %d\n", sum, curr_val);
                    sum = sum - curr_val;
                    break;
                case 12 :
                    //printf("%f * %d\n", sum, curr_val);
                    sum = sum * curr_val;
                    break;
                case 13 :
                    if( curr_val > 0 )
                    {
                        //printf("%f / %d\n", sum, curr_val);
                        sum = sum / curr_val;
                    }
                    break;
                default :
                    break;
            }

            last_op = gene;
            curr_val = 0;
        }
    }

    return fabs( sum - ((double)target_val) );
}

int compare_chromosomes( const void *x, const void *y )
{
    double f = ((chromosome*)x)->fitness;
    double s = ((chromosome*)y)->fitness;

    if (f > s) return  1;
    if (f < s) return -1;

    return 0;
}

char* brain( int target )
{
    char * retval;
    int is_solved = 0;

    chromosome* population;
    population = malloc( NUM_CHROMOSOMES * sizeof(chromosome *) );

    while( !is_solved )
    {
        printf("generate %d random chromos\n", NUM_CHROMOSOMES);
        // generate random chromosomes
        for( int i = 0; i < NUM_CHROMOSOMES; i++ )
        {
            printf("start %d, %d, %d\n",i, NUM_CHROMOSOMES, !!is_solved);
            byte* chromosome = get_chromosome();

            // evaluate each chromosome and assign a fitness score
            print_chromosome( chromosome );
            population[i].bits = chromosome;
            printf("fitness start\n");
            population[i].fitness = evaluate_fitness( target, population[i].bits );
            printf("fitness end\n");
        }

        printf("qsort\n");
        qsort( population, NUM_CHROMOSOMES, sizeof( chromosome * ), compare_chromosomes );

        printf("qsort\n");
        for( int i = 0; i < NUM_CHROMOSOMES; i++ )
            print_chromosome( population[i].bits );
        // select two members from the population
        // swap genes with crossover rate 0.7
        // create new chromosomes by mutating random genes in resultant chromosome
        is_solved = 1;
    }

    for( int i = 0; i < NUM_CHROMOSOMES; i++ )
    {
        printf( "%d\n", i );
        free( population[i].bits );
    }

    free(population);

    return retval;
}

int main ( int argc, char *argv[] )
{
    if ( argc != 2 ) /* argc should be 2 for correct execution */
    {
        /* We print argv[0] assuming it is the program name */
        printf( "usage: %s <number>\n", argv[0] );
    }
    else
    {
        printf( "running genetic algorithm to find mathematical expression for %s\n", argv[1] );
        srand(time(NULL));
        char* retval = brain( (int)argv[0] );
        printf( "genetic algorithm found %s = %s\n", argv[1], argv[0] );
    }
}
